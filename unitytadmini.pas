unit UnitYtAdmini;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  StdCtrls, httpsend, ssl_openssl, synacode, XMLRead, XMLWrite, DOM;

type

  { TfrmYtAdmini }

  TfrmYtAdmini = class(TForm)
    btnOpen: TButton;
    btnStart: TButton;
    btnPause: TButton;
    btnFix: TButton;
    cmbProjects: TComboBox;
    cmbIssues: TComboBox;
    lblLoginName: TLabel;
    lblIssueTitle: TLabel;
    Label2: TLabel;
    lblIssue: TLabel;
    lblState: TLabel;
    lblIssueChoose: TLabel;
    lblProjectChoose: TLabel;
    procedure btnChangeClick(Sender: TObject);
    procedure cmbIssuesChange(Sender: TObject);
    procedure cmbIssuesSelect(Sender: TObject);
    procedure cmbProjectsChange(Sender: TObject);
    procedure cmbProjectsSelect(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    var User,
        Pwd,
        YtHttp,
        YtProtocol : string;
    function YoutrackLogin(): string;
   // function GetUserName() : string;
    function GetProjects() : TXMLDocument;
    function GetIssues(ProjectShortName : string) : TXMLDocument;
    function GetIssue(IssueShortName : string) : TXMLDocument;
    function GetIssueState(issue : TXMLDocument) : String;
    function SetIssueState(IssueShortName, state : string) : string;
    procedure SetStateColor(IssueState : string);
    procedure UpdateComboProjects(projects : TXMLDocument);
    procedure UpdateComboIssues(issues : TXMLDocument);
  public
    { public declarations }
  end;

var
  frmYtAdmini: TfrmYtAdmini;

implementation
Uses UnitYtLogin;

{$R *.lfm}

{ TfrmYtAdmini }

CONST SUBMITTED = clAqua;
CONST OPEN = clRed;
CONST INPROGRESS = clYellow;
CONST PAUSED = clPurple;
CONST FIXED = clMoneyGreen;

var
  YtClient : THTTPSend;
  ProjectShort : Array of String;
  IssueShort : Array of String;
  ProjectIndex,
  IssueIndex : integer;
  StateList : TStringList;

function TfrmYtAdmini.YoutrackLogin() : string;
var
  response : Boolean;
  ResponseList : TStringList;
  ResponseDoc : TMemoryStream;
  Params : String;
begin
  ResponseList := TStringList.Create;
  YtClient := THTTPSend.Create;
  ResponseDoc := TMemoryStream.Create;
  if YtProtocol = 'https://' then begin
    YtClient.Sock.CreateWithSSL(TSSLOpenSSL);
    YtClient.Sock.SSLDoConnect;
  end;
  with YtClient do
    try
      Clear;
      Params := 'login=' + EncodeURLElement(User) + '&' +
                'password=' + EncodeURLElement(Pwd);
      Document.Write(Pointer(Params)^, Length(Params));
      MimeType := 'application/x-www-form-urlencoded';
      response := HTTPMethod('POST', YtProtocol + YtHttp + '/rest/user/login');
      ResponseDoc.CopyFrom(Document, 0);
      if response = true then
      begin
        ResponseDoc.Seek(0,soFromBeginning);
        ResponseList.LoadFromStream(ResponseDoc);
        Result := ResponseList.Text;
      end;
    finally
      ResponseList.Free;
    end;
end;

{ TOMORROW (08/10/15)
function TfrmYtAdmini.GetUserName() : string;
var
  S : TStringStream;
  XML : TXMLDocument;
  response : string;
begin
  with YtClient do
    try
      response := Get(YtProtocol + YtHttp + '/rest/user/current');
      S:= TStringStream.Create(response);
      S.Position:=0;
      XML:=Nil;
      ReadXMLFile(XML, S);

      if XML.DocumentElement.ChildNodes.Item[0].HasAttributes then
      begin
        Result := XML.DocumentElement.ChildNodes.Item[0].Attributes[2].NodeValue;
      end
      else
       Result := '';
    finally
    end;
end;     }

function TfrmYtAdmini.GetProjects() : TXMLDocument;
var
  S : TStringStream;
  XML : TXMLDocument;
  response : boolean;
  ResponseList : TStringList;
begin
  with YtClient do
    try
      Clear;
      ResponseList := TStringList.Create;
      response := HttpMethod('GET', YtProtocol + YtHttp + '/rest/project/all?true');
      if response = true then begin
        ResponseList.LoadFromStream(YtClient.Document);
        S:= TStringStream.Create(ResponseList.Text);
        S.Position:=0;
        XML:=Nil;
        ReadXMLFile(XML,S);
        Result := XML;
      end;
    finally
      ResponseList.Free;
    end;
end;

function TfrmYtAdmini.GetIssueState(issue : TXMLDocument) : string;
  function ProcessIssue(Node : TXMLDocument) : string;
  var
    ArrayLength : integer;
    nodeValue : string;
    cNode : TDOMNode;

    procedure ProcessIssueField(Node : TDOMNode);
    var
      IssueSummary,
      IssueState : string;
    begin
      if Node = nil then Exit;

      if Node.HasAttributes then
      begin
        nodeValue := Node.Attributes[0].NodeValue;
        if nodeValue = 'summary' then
        begin
          IssueSummary := Node.FirstChild.FirstChild.NodeValue;
          lblIssue.Caption:= IssueSummary;
        end
        else if nodeValue = 'State' then
        begin
          IssueState := Node.FirstChild.FirstChild.NodeValue;
          lblState.Caption := IssueState;
          SetStateColor(IssueState);
          Result := IssueState;
        end;
      end;
    end;

  begin
    if Node = nil then Exit;

    if Node.HasAttributes then
    begin
      ArrayLength := Length(IssueShort);
      SetLength(IssueShort, ArrayLength + 1);
      IssueShort[ArrayLength] := Node.Attributes[0].NodeValue;
    end;

    cNode := Node.ChildNodes.Item[0].ChildNodes.Item[0];

    while cNode <> nil do
    begin
      ProcessIssueField(cNode);
      cNode := cNode.NextSibling;
    end;


  end;

begin
  Result := ProcessIssue(issue);
end;


function TfrmYtAdmini.GetIssues(ProjectShortName : string) : TXMLDocument;
var
  S : TStringStream;
  XML : TXMLDocument;
  response : boolean;
  ResponseList : TStringList;
  RequestString :string;
begin
  with YtClient do
    try
      Clear;
      ResponseList := TStringList.Create;
      RequestString := YtProtocol + YtHttp + '/rest/issue/byproject/' + ProjectShortName + '?max=999';
      response := HttpMethod('GET', RequestString);
      if response = true then
      begin
        ResponseList.LoadFromStream(Document);
        if Pos('<error>', ResponseList.Text) = 0 then
        begin
          S:= TStringStream.Create(ResponseList.Text);
          S.Position:=0;
          XML:=Nil;
          ReadXMLFile(XML,S);
          Result := XML;
        end
        else Result:=Nil;
      end;
    finally
      ResponseList.Free;
    end;

end;

function TfrmYtAdmini.GetIssue(IssueShortName : string) : TXMLDocument;
var
  S : TStringStream;
  XML : TXMLDocument;
  response : boolean;
  ResponseList : TStringList;
begin
  with YtClient do
    try
      Clear;
      ResponseList := TStringList.Create;
      response := HttpMethod('GET', YtProtocol + YtHttp + '/rest/issue/' + IssueShortName);
      if response = true then
      begin
        ResponseList.LoadFromStream(Document);
        S:= TStringStream.Create(ResponseList.Text);
        S.Position:=0;
        XML:=Nil;
        ReadXMLFile(XML,S);
        Result := XML;
      end;
    finally
    end;
end;

function TfrmYtAdmini.SetIssueState(IssueShortName, state : string) : string;
var
  PostStateUrl : string;
  response : boolean;
  ResponseList : TStringList;
begin
  with YtClient do
    try
      Clear;
      PostStateUrl := YtProtocol + YtHttp + '/rest/issue/' + IssueShortName + '/execute?command=State%20' + StringReplace(state, ' ', '%20', [rfReplaceAll]) + '&runAs=' + User;
      ResponseList := TStringList.Create;
      response := HttpMethod('POST', PostStateUrl);
      if response = true then
      begin
        ResponseList.LoadFromStream(YtClient.Document);
        Result := ResponseList.Text;
      end;
    finally
    end;
end;

procedure TfrmYtAdmini.UpdateComboProjects(projects : TXMLDocument);
var
  iNode: TDOMNode;

  procedure ProcessNode(Node: TDOMNode);
  var
    cNode: TDOMNode;
    var ArrayLength : integer;
  begin
    if Node = nil then Exit;

    if Node.HasAttributes then
    begin
      cmbProjects.Items.Add(Node.Attributes[1].NodeValue + ' : ' + Node.Attributes[0].NodeValue);
      ArrayLength := Length(ProjectShort);
      SetLength(ProjectShort, ArrayLength + 1);
      ProjectShort[ArrayLength] := Node.Attributes[1].NodeValue;
    end;

    cNode := Node.ChildNodes.Item[0];

    while cNode <> nil do
    begin
      ProcessNode(cNode);
      cNode := cNode.NextSibling;
    end;
  end;

begin
  cmbProjects.Clear;
  SetLength(ProjectShort, 0);
  iNode := projects.DocumentElement.ChildNodes.Item[0];
  while iNode <> nil do
  begin
    ProcessNode(iNode);
    iNode := iNode.NextSibling;
  end;
end;

procedure TfrmYtAdmini.UpdateComboIssues(issues : TXMLDocument);
var
  iNode: TDOMNode;

  procedure ProcessIssue(Node: TDOMNode);
  var
    cNode: TDOMNode;
    ArrayLength : integer;
    nodeValue : string;

    procedure ProcessIssueField(IssueName : string; Node : TDOMNode);
    var IssueSummary : string;
    begin
      if Node = nil then Exit;

      if Node.HasAttributes then
      begin
        nodeValue := Node.Attributes[0].NodeValue;
        if nodeValue = 'summary' then
        begin
          IssueSummary := Node.FirstChild.FirstChild.NodeValue;
          cmbIssues.Items.Add(IssueName + ' : ' + IssueSummary);
        end;
      end;
    end;

  begin
    if Node = nil then Exit;

    if Node.HasAttributes then
    begin
      ArrayLength := Length(IssueShort);
      SetLength(IssueShort, ArrayLength + 1);
      IssueShort[ArrayLength] := Node.Attributes[0].NodeValue;
    end;

    cNode := Node.ChildNodes.Item[0];

    while cNode <> nil do
    begin
      ProcessIssueField(Node.Attributes[0].NodeValue, cNode);
      cNode := cNode.NextSibling;
    end;
  end;

begin
  cmbIssues.Clear;
  SetLength(IssueShort, 0);
  iNode := issues.DocumentElement.ChildNodes.Item[0];
  while iNode <> nil do
  begin
    ProcessIssue(iNode);
    iNode := iNode.NextSibling;
  end;
end;

procedure TfrmYtAdmini.FormShow(Sender: TObject);
var response : string;
    XMLresponse: TXMLDocument;
begin
  User := frmYtLogin.User;
  Pwd := frmYtLogin.Pwd;
  YtHttp := frmYtLogin.YtHttp;
  YtProtocol := frmYtLogin.YtProtocol;
  response := YoutrackLogin();

  if Pos('error', response) = 0 then             //'<login>ok</login>'
  begin
    //lblLoginName.Caption := 'Logged In as: ' + GetUserName();

    XMLresponse := GetProjects();
    UpdateComboProjects(XMLresponse);
    StateList := TStringList.Create();
    StateList.Add('Submitted');
    StateList.Add('Open');
    StateList.Add('In Progress');
    StateList.Add('Paused');
    StateList.Add('Fixed');
  end else
  begin
    ShowMessage(response);
  end;
end;

procedure TfrmYtAdmini.SetStateColor(IssueState : string);
begin
  case StateList.IndexOf(IssueState) of
    0 : lblState.Color := SUBMITTED;
    1 : lblState.Color := OPEN;
    2 : lblState.Color := INPROGRESS;
    3 : lblState.Color := PAUSED;
    4 : lblState.Color := FIXED;
  end;
end;

procedure TfrmYtAdmini.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  YtClient.Free();
  StateList.Free();
  cmbProjects.Clear();
  cmbIssues.Clear();
  frmYtAdmini.Hide();
  frmYtLogin.Show();
end;

procedure TfrmYtAdmini.cmbProjectsChange(Sender: TObject);
begin
  ProjectIndex := cmbProjects.ItemIndex;
end;

procedure TfrmYtAdmini.cmbProjectsSelect(Sender: TObject);
var
  XMLresponse: TXMLDocument;
begin
  if ProjectIndex <> -1 then
  begin
    XMLresponse := GetIssues(ProjectShort[ProjectIndex]);
    if XMLresponse <> Nil then
       UpdateComboIssues(XMLresponse);
  end;
end;

procedure TfrmYtAdmini.cmbIssuesChange(Sender: TObject);
begin
  IssueIndex := cmbIssues.ItemIndex;
end;

procedure TfrmYtAdmini.btnChangeClick(Sender: TObject);
var
  response : string;
begin
  if IssueIndex <> -1 then
  begin
    response := SetIssueState(IssueShort[IssueIndex],
                              TButton(Sender).Caption);
    if response <> ''  then
    begin
       ShowMessage('There was an Error: ' + response);
    end
    else begin
      lblState.caption := TButton(Sender).Caption;
      SetStateColor(TButton(Sender).Caption);
    end;

  end;
end;

procedure TfrmYtAdmini.cmbIssuesSelect(Sender: TObject);
var
  XMLresponse: TXMLDocument;
begin
  if IssueIndex <> -1 then
  begin
    XMLresponse := GetIssue(IssueShort[IssueIndex]);
    lblState.caption := GetIssueState(XMLResponse);
  end;
end;

end.

