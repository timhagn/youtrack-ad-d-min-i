program YtAdmin;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, UnitYtLogin, UnitYtAdmini
  { you can add units after this };

{$R *.res}

begin
  Application.Title:='Youtrack Ad(d)-min/i';
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TfrmYtLogin, frmYtLogin);
  Application.CreateForm(TfrmYtAdmini, frmYtAdmini);
  Application.Run;
end.

