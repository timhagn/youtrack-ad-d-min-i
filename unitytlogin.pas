unit UnitYtLogin;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  XMLRead, XMLWrite, DOM;

type

  { TfrmYtLogin }

  TfrmYtLogin = class(TForm)
    btnLogin: TButton;
    cmbProtocol: TComboBox;
    edtUser: TEdit;
    edtPwd: TEdit;
    edtYtHttp: TEdit;
    lblUsername: TLabel;
    lblPassword: TLabel;
    lblYoutrackHttp: TLabel;
    procedure btnLoginClick(Sender: TObject);
    procedure edtPwdKeyPress(Sender: TObject; var Key: char);
    procedure edtUserKeyPress(Sender: TObject; var Key: char);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    procedure SaveConfig();
  public
    var User,
        Pwd,
        YtHttp,
        YtProtocol: string;
    { public declarations }
  end;

var
  frmYtLogin: TfrmYtLogin;

implementation
Uses UnitYtAdmini;

{$R *.lfm}

{ TfrmYtLogin }

procedure TfrmYtLogin.btnLoginClick(Sender: TObject);
begin
  if (edtUser.ToString <> '') and (edtPwd.ToString <> '') and (edtYtHttp.ToString <> '') then
  begin
    User := edtUser.Text;
    Pwd := edtPwd.Text;
    YtHttp := edtYtHttp.Text;
    YtProtocol := cmbProtocol.Text;
    SaveConfig();
    frmYtLogin.Hide();
    frmYtAdmini.Show();
  end;
end;

procedure TfrmYtLogin.edtPwdKeyPress(Sender: TObject; var Key: char);
begin
  if Key = #13 then
     if (edtUser.Text <> '') and (edtYtHttp.Text <> '') and (edtPwd.Text <> '') then
        btnLogin.Click
     else
       edtYtHttp.SetFocus;

end;

procedure TfrmYtLogin.edtUserKeyPress(Sender: TObject; var Key: char);
begin
  if Key = #13 then
     edtPwd.SetFocus;
end;


procedure TfrmYtLogin.FormShow(Sender: TObject);
var
  ConfigXML : TXMLDocument;
begin
  if FileExists('YtAdmini.xml') then
    try
      ReadXMLFile(ConfigXML, 'YtAdmini.xml');
      edtUser.Text := ConfigXML.DocumentElement.FindNode('user').FirstChild.NodeValue;
      cmbProtocol.ItemIndex := cmbProtocol.Items.IndexOf(ConfigXML.DocumentElement.FindNode('protocol').FirstChild.NodeValue);
      edtYtHttp.Text := ConfigXML.DocumentElement.FindNode('http').FirstChild.NodeValue;
      edtPwd.SetFocus;
    finally
     ConfigXML.Free;
    end;
end;

procedure TfrmYtLogin.SaveConfig();
var
  ConfigXML : TXMLDocument;
  RootNode, configNode, configText : TDOMNode;
begin
  try
    ConfigXML := TXMLDocument.Create;
    RootNode := ConfigXML.CreateElement('ytadmin');
    ConfigXML.AppendChild(RootNode);
    RootNode := ConfigXML.DocumentElement;
    configNode := ConfigXML.CreateElement('user');
    configText := ConfigXML.CreateTextNode(User);
    configNode.AppendChild(configText);
    RootNode.AppendChild(configNode);
    configNode := ConfigXML.CreateElement('protocol');
    configText := ConfigXML.CreateTextNode(YtProtocol);
    configNode.AppendChild(configText);
    RootNode.AppendChild(configNode);
    configNode := ConfigXML.CreateElement('http');
    configText := ConfigXML.CreateTextNode(YtHttp);
    configNode.AppendChild(configText);
    RootNode.AppendChild(configNode);

    writeXMLFile(ConfigXML, 'YtAdmini.xml');
  finally
  end;
end;

end.

