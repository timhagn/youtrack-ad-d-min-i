unit UnitYtAdmin;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  StdCtrls, fphttpclient, XMLRead, DOM;

type

  { TfrmYtAdmin }

  TfrmYtAdmin = class(TForm)
    btnOpen: TButton;
    btnStart: TButton;
    btnPause: TButton;
    btnFix: TButton;
    cmbProjects: TComboBox;
    cmbIssues: TComboBox;
    lblIssueTitle: TLabel;
    Label2: TLabel;
    lblIssue: TLabel;
    lblState: TLabel;
    lblIssueChoose: TLabel;
    lblProjectChoose: TLabel;
    procedure btnChangeClick(Sender: TObject);
    procedure cmbIssuesChange(Sender: TObject);
    procedure cmbIssuesSelect(Sender: TObject);
    procedure cmbProjectsChange(Sender: TObject);
    procedure cmbProjectsSelect(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    var User,
        Pwd,
        YtHttp : string;
    function YoutrackLogin(): string;
    function GetProjects() : TXMLDocument;
    function GetIssues(ProjectShortName : string) : TXMLDocument;
    function GetIssue(IssueShortName : string) : TXMLDocument;
    function GetIssueState(issue : TXMLDocument) : String;
    function SetIssueState(IssueShortName, state : string) : string;
    procedure SetStateColor(IssueState : string);
    procedure UpdateComboProjects(projects : TXMLDocument);
    procedure UpdateComboIssues(issues : TXMLDocument);
  public
    { public declarations }
  end;

var
  frmYtAdmin: TfrmYtAdmin;

implementation
Uses UnitYtLogin;

{$R *.lfm}

{ TfrmYtAdmin }

CONST SUBMITTED = clAqua;
CONST OPEN = clRed;
CONST INPROGRESS = clYellow;
CONST PAUSED = clPurple;
CONST FIXED = clMoneyGreen;

var
  ProjectShort : Array of String;
  IssueShort : Array of String;
  ProjectIndex,
  IssueIndex : integer;
  StateList : TStringList;

function TfrmYtAdmin.YoutrackLogin() : string;
begin
  with TFPHttpClient.Create(Nil) do
    try
      Result := Post('http://' + YtHttp + '/rest/user/login?login=' + User + '&password=' + Pwd);
  finally
    Free;
  end;
end;

function TfrmYtAdmin.GetProjects() : TXMLDocument;
var
  S : TStringStream;
  XML : TXMLDocument;
  response : string;
begin
  with TFPHttpClient.Create(Nil) do
    try
      response := Get('http://' + YtHttp + '/rest/project/all?true');
      S:= TStringStream.Create(response);
      S.Position:=0;
      XML:=Nil;
      ReadXMLFile(XML,S);
      Result := XML;
  finally
    Free;
  end;
end;

function TfrmYtAdmin.GetIssueState(issue : TXMLDocument) : string;
  function ProcessIssue(Node : TXMLDocument) : string;
  var
    ArrayLength : integer;
    nodeValue : string;
    cNode : TDOMNode;

    procedure ProcessIssueField(Node : TDOMNode);
    var
      IssueSummary,
      IssueState : string;
    begin
      if Node = nil then Exit; // Aufhören, wenn ein Blatt erreicht ist
        // Einen Knoten zum Baum hinzufügen
      if Node.HasAttributes then
      begin
        nodeValue := Node.Attributes[0].NodeValue;
        if nodeValue = 'summary' then
        begin
          IssueSummary := Node.FirstChild.FirstChild.NodeValue;
          lblIssue.Caption:= IssueSummary;
        end
        else if nodeValue = 'State' then
        begin
          IssueState := Node.FirstChild.FirstChild.NodeValue;
          lblState.Caption := IssueState;
          SetStateColor(IssueState);
          Result := IssueState;
        end;
      end;
    end;

  begin
    if Node = nil then Exit; // Aufhören, wenn ein Blatt erreicht ist

    // Einen Knoten zum Baum hinzufügen
    if Node.HasAttributes then
    begin
      ArrayLength := Length(IssueShort);
      SetLength(IssueShort, ArrayLength + 1);
      IssueShort[ArrayLength] := Node.Attributes[0].NodeValue;
    end;

    // Zum Kindknoten weiter gehen
    cNode := Node.ChildNodes.Item[0].ChildNodes.Item[0];

    // Alle Kindknoten bearbeiten
    while cNode <> nil do
    begin
      ProcessIssueField(cNode);
      cNode := cNode.NextSibling;
    end;


  end;

begin
  Result := ProcessIssue(issue);
end;


function TfrmYtAdmin.GetIssues(ProjectShortName : string) : TXMLDocument;
var
  S : TStringStream;
  XML : TXMLDocument;
  response : string;
begin
  with TFPHttpClient.Create(Nil) do
    try
      response := Get('http://' + YtHttp + '/rest/issue/byproject/' + ProjectShortName);
      S:= TStringStream.Create(response);
      S.Position:=0;
      XML:=Nil;
      ReadXMLFile(XML,S);
      Result := XML;
  finally
    Free;
  end;
end;

function TfrmYtAdmin.GetIssue(IssueShortName : string) : TXMLDocument;
var
  S : TStringStream;
  XML : TXMLDocument;
  response : string;
begin
  with TFPHttpClient.Create(Nil) do
    try
      response := Get('http://' + YtHttp + '/rest/issue/' + IssueShortName);
      S:= TStringStream.Create(response);
      S.Position:=0;
      XML:=Nil;
      ReadXMLFile(XML,S);
      Result := XML;
  finally
    Free;
  end;
end;

function TfrmYtAdmin.SetIssueState(IssueShortName, state : string) : string;
var
  PostStateUrl :string;
begin
  with TFPHttpClient.Create(Nil) do
    try
      // TODO: Morgen nochmal schauen, wieso es per Postmaster geht,
      // aber hier nicht...
      PostStateUrl := 'http://' + YtHttp + '/rest/issue/' + IssueShortName + '/execute?command=State%20' + StringReplace(state, ' ', '%20', [rfReplaceAll]);
      Result := SimplePost(PostStateUrl);
  finally
    Free;
  end;
end;

procedure TfrmYtAdmin.UpdateComboProjects(projects : TXMLDocument);
var
  iNode: TDOMNode;

  procedure ProcessNode(Node: TDOMNode);
  var
    cNode: TDOMNode;
    var ArrayLength : integer;
  begin
    if Node = nil then Exit; // Aufhören, wenn ein Blatt erreicht ist

    // Einen Knoten zum Baum hinzufügen
    if Node.HasAttributes then
    begin
      cmbProjects.Items.Add(Node.Attributes[1].NodeValue + ' : ' + Node.Attributes[0].NodeValue);
      ArrayLength := Length(ProjectShort);
      SetLength(ProjectShort, ArrayLength + 1);
      ProjectShort[ArrayLength] := Node.Attributes[1].NodeValue;
    end;

    // Zum Kindknoten weiter gehen
    cNode := Node.ChildNodes.Item[0];

    // Alle Kindknoten bearbeiten
    while cNode <> nil do
    begin
      ProcessNode(cNode);
      cNode := cNode.NextSibling;
    end;
  end;

begin
  iNode := projects.DocumentElement.ChildNodes.Item[0];
  while iNode <> nil do
  begin
    ProcessNode(iNode); // Rekursiv
    iNode := iNode.NextSibling;
  end;
end;

procedure TfrmYtAdmin.UpdateComboIssues(issues : TXMLDocument);
var
  iNode: TDOMNode;

  procedure ProcessIssue(Node: TDOMNode);
  var
    cNode: TDOMNode;
    ArrayLength : integer;
    nodeValue : string;

    procedure ProcessIssueField(IssueName : string; Node : TDOMNode);
    var IssueSummary : string;
    begin
      if Node = nil then Exit; // Aufhören, wenn ein Blatt erreicht ist
        // Einen Knoten zum Baum hinzufügen
      if Node.HasAttributes then
      begin
        nodeValue := Node.Attributes[0].NodeValue;
        if nodeValue = 'summary' then
        begin
          IssueSummary := Node.FirstChild.FirstChild.NodeValue;
          cmbIssues.Items.Add(IssueName + ' : ' + IssueSummary);
        end;
      end;
    end;

  begin
    if Node = nil then Exit; // Aufhören, wenn ein Blatt erreicht ist

    // Einen Knoten zum Baum hinzufügen
    if Node.HasAttributes then
    begin
      ArrayLength := Length(IssueShort);
      SetLength(IssueShort, ArrayLength + 1);
      IssueShort[ArrayLength] := Node.Attributes[0].NodeValue;
    end;

    // Zum Kindknoten weiter gehen
    cNode := Node.ChildNodes.Item[0];

    // Alle Kindknoten bearbeiten
    while cNode <> nil do
    begin
      ProcessIssueField(Node.Attributes[0].NodeValue, cNode);
      cNode := cNode.NextSibling;
    end;
  end;

begin
  iNode := issues.DocumentElement.ChildNodes.Item[0];
  while iNode <> nil do
  begin
    ProcessIssue(iNode);
    iNode := iNode.NextSibling;
  end;
end;

procedure TfrmYtAdmin.FormShow(Sender: TObject);
var response : string;
    XMLresponse: TXMLDocument;
begin
  User := frmYtLogin.User;
  Pwd := frmYtLogin.Pwd;
  YtHttp := frmYtLogin.YtHttp;
  response := YoutrackLogin();
  if response = '<login>ok</login>' then
  begin
    XMLresponse := GetProjects();
    UpdateComboProjects(XMLresponse);
    StateList := TStringList.Create();
    StateList.Add('Submitted');
    StateList.Add('Open');
    StateList.Add('In Progress');
    StateList.Add('Paused');
    StateList.Add('Fixed');
  end;
end;

procedure TfrmYtAdmin.SetStateColor(IssueState : string);
begin
  case StateList.IndexOf(IssueState) of
    0 : lblState.Color := SUBMITTED;
    1 : lblState.Color := OPEN;
    2 : lblState.Color := INPROGRESS;
    3 : lblState.Color := PAUSED;
    4 : lblState.Color := FIXED;
  end;
end;

procedure TfrmYtAdmin.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  StateList.Free();
  cmbProjects.Clear();
  cmbIssues.Clear();
  frmYtAdmin.Hide();
  frmYtLogin.Show();
end;

procedure TfrmYtAdmin.cmbProjectsChange(Sender: TObject);
begin
  ProjectIndex := cmbProjects.ItemIndex;
end;

procedure TfrmYtAdmin.cmbProjectsSelect(Sender: TObject);
var
  XMLresponse: TXMLDocument;
begin
  if ProjectIndex <> -1 then
  begin
    XMLresponse := GetIssues(ProjectShort[ProjectIndex]);
    UpdateComboIssues(XMLresponse);
  end;
end;

procedure TfrmYtAdmin.cmbIssuesChange(Sender: TObject);
begin
  IssueIndex := cmbIssues.ItemIndex;
end;

procedure TfrmYtAdmin.btnChangeClick(Sender: TObject);
var
  response : string;
begin
  if IssueIndex <> -1 then
  begin
    response := SetIssueState(IssueShort[IssueIndex],
                              TButton(Sender).Caption);
    lblState.caption := TButton(Sender).Caption;
    SetStateColor(TButton(Sender).Caption);

  end;
end;

procedure TfrmYtAdmin.cmbIssuesSelect(Sender: TObject);
var
  XMLresponse: TXMLDocument;
begin
  if IssueIndex <> -1 then
  begin
    XMLresponse := GetIssue(IssueShort[IssueIndex]);
    lblState.caption := GetIssueState(XMLResponse);
  end;
end;

end.

